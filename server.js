var PORT = process.env.PORT || 8080;

var express = require('express');
var app = express();
var server = require('http').createServer(app);
var request = require('request');
var fs = require('fs');
var _ = require('underscore');
var mustache = require('mustache');
var moment = require('moment');

var oneDay = 86400000;
app.use(express.static('public', { maxAge: oneDay }));

var products = {
   "MGAC2B/A":"iPhone 6 Plus 128GB Space Grey",
   "MGAF2B/A":"iPhone 6 Plus 128GB Gold",
   "MGAE2B/A":"iPhone 6 Plus 128GB Silver",
   "MGAH2B/A":"iPhone 6 Plus 64GB Space Grey",
   "MGAK2B/A":"iPhone 6 Plus 64GB Gold",
   "MGAJ2B/A":"iPhone 6 Plus 64GB Silver",
   "MGA82B/A":"iPhone 6 Plus 16GB Space Grey",
   "MGAA2B/A":"iPhone 6 Plus 16GB Gold",
   "MGA92B/A":"iPhone 6 Plus 16GB Silver",
   "MG4A2B/A":"iPhone 6 128GB Space Grey",
   "MG4E2B/A":"iPhone 6 128GB Gold",
   "MG4C2B/A":"iPhone 6 128GB Silver",
   "MG4F2B/A":"iPhone 6 64GB Space Grey",
   "MG4J2B/A":"iPhone 6 64GB Gold",
   "MG4H2B/A":"iPhone 6 64GB Silver",
   "MG472B/A":"iPhone 6 16GB Space Grey",
   "MG492B/A":"iPhone 6 16GB Gold",
   "MG482B/A":"iPhone 6 16GB Silver"
};

var stores = {
    "R227":{
        "name":"Bentall Centre, Kingston"
    },
    "R113":{
        "name":"Bluewater, Greenhithe"
    },
    "R340":{
        "name":"Braehead, Glasgow"
    },
    "R163":{
        "name":"Brent Cross, Barnet"
    },
    "R496":{
        "name":"Bromley"
    },
    "R135":{
        "name":"Buchanan Street, Glasgow"
    },
    "R118":{
        "name":"Bullring, Birmingham"
    },
    "R252":{
        "name":"Cabot Circus, Bristol"
    },
    "R391":{
        "name":"Chapelfield, Norwich"
    },
    "R244":{
        "name":"Churchill Square, Brighton"
    },
    "R245":{
        "name":"Covent Garden, London"
    },
    "R393":{
        "name":"Cribbs Causeway, Bristol"
    },
    "R545":{
        "name":"Drake Circus, Plymouth"
    },
    "R341":{
        "name":"Eldon Square, Newcastle"
    },
    "R482":{
        "name":"Festival Place, Basingstoke"
    },
    "R270":{
        "name":"Grand Arcade, Cambridge"
    },
    "R308":{
        "name":"Highcross, Leicester"
    },
    "R242":{
        "name":"Lakeside, Grays"
    },
    "R239":{
        "name":"Liverpool ONE"
    },
    "R215":{
        "name":"Manchester Arndale"
    },
    "R153":{
        "name":"Meadowhall, Sheffield"
    },
    "R423":{
        "name":"Metrocentre, Gateshead"
    },
    "R269":{
        "name":"Milton Keynes"
    },
    "R279":{
        "name":"Princesshay, Exeter",
        "link":"https://www.apple.com/uk/retail/princesshay/"
    },
    "R092":{
        "name":"Regent Street, London"
    },
    "R335":{
        "name":"SouthGate, Bath"
    },
    "R334":{
        "name":"St David's 2, Cardiff"
    },
    "R410":{
        "name":"Stratford City"
    },
    "R176":{
        "name":"The Oracle, Reading"
    },
    "R255":{
        "name":"Touchwood Centre, Solihull"
    },
    "R136":{
        "name":"Trafford Centre, Manchester"
    },
    "R372":{
        "name":"Trinity Leeds"
    },
    "R363":{
        "name":"Union Square, Aberdeen"
    },
    "R313":{
        "name":"Victoria Square, Belfast"
    },
    "R527":{
        "name":"Watford"
    },
    "R174":{
        "name":"WestQuay, Southampton"
    },
    "R226":{
        "name":"White City, London"
    }
};

var availability = {};

app.get('/', function(req, res) {
    console.log('GET /');
	fs.readFile(__dirname + '/views/index.html', function (err, data) {
  		if (err) {
    		console.warn("Error getting /views/index.html", err);
    		res.status(503).send("Whoops - something has gone a bit wrong - please try again later");
	        return;
  		}
  		var availabilityView = {};
  		availabilityView.evenAvailability = _.filter(availability.availability, function(val, index) {
  			return index % 2 == 0;
  		});
  		availabilityView.oddAvailability = _.filter(availability.availability, function(val, index) {
  			return index % 2 != 0;
  		});
  		if (availability.updated) {
  			availabilityView.updated = moment(availability.updated).zone("+01:00").format('MMMM Do YYYY, h:mm a');
  		}
  		availabilityView.availability = availability.availability.length > 0;

  		res.send(mustache.to_html(data.toString(), availabilityView));
	});
});

app.get('/api/availability', function(req, res) {
    console.log('GET /api/availability');
    res.set('Content-Type', 'application/json');
    res.send(JSON.stringify(availability));
});

function loadAppleAvailability(url) {
	var url = "https://reserve.cdn-apple.com/GB/en_GB/reserve/iPhone/availability.json";
	//var url = "http://localhost:8080/availability.json";
	console.log("Loading appleAvailability from", url);
    request(url, function (err, httpResponse, body) {
	    var response = {
	    	availability: []
	    };
    	if (err) {
    		console.warn("Error getting availability.json", err);
	        return;
	    }
	    var appleAvailability = JSON.parse(body);
	    for (var storeId in appleAvailability) {
	    	var productsAtStore = appleAvailability[storeId];
	    	var availableProductsAtStore = [];
	    	for (var productId in productsAtStore) {
	    		if (productsAtStore[productId]) {
	    			availableProductsAtStore.push(products[productId]);
	    		}
	    	}
	    	if (availableProductsAtStore.length > 0) {
          var store = stores[storeId];
          if (store) {
	    		  response.availability.push({store: stores[storeId].name, products: availableProductsAtStore});
          } else {
            console.warn("Unknown storeId", storeId);
          }
	    	}
	    }
	    response.updated = appleAvailability.updated || new Date().valueOf();
	    availability = response;
	});
}

loadAppleAvailability();
setInterval(function() {
	loadAppleAvailability();
}, 60000);

server.listen(PORT);
console.log('Listening on port ' + PORT);
